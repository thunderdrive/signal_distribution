EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ThunderDrive
LIBS:signal_distribution-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 7
Title "Connector to logic board"
Date "2017-03-20"
Rev "1"
Comp "ThunderDrive"
Comment1 "har-flex 26 pin straight"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L +3.3V #PWR037
U 1 1 58D2A664
P 5800 3750
F 0 "#PWR037" H 5800 3600 50  0001 C CNN
F 1 "+3.3V" H 5800 3890 50  0000 C CNN
F 2 "" H 5800 3750 50  0001 C CNN
F 3 "" H 5800 3750 50  0001 C CNN
	1    5800 3750
	1    0    0    -1  
$EndComp
$Comp
L +3.3VADC #PWR038
U 1 1 58D2A670
P 6800 3700
F 0 "#PWR038" H 6950 3650 50  0001 C CNN
F 1 "+3.3VADC" H 6800 3800 50  0000 C CNN
F 2 "" H 6800 3700 50  0001 C CNN
F 3 "" H 6800 3700 50  0001 C CNN
	1    6800 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1400 4350 1400
Wire Wire Line
	4350 1200 4800 1200
$Comp
L GNDA #PWR039
U 1 1 58D2A67A
P 4600 4300
F 0 "#PWR039" H 4600 4050 50  0001 C CNN
F 1 "GNDA" H 4600 4150 50  0000 C CNN
F 2 "" H 4600 4300 50  0001 C CNN
F 3 "" H 4600 4300 50  0001 C CNN
	1    4600 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1600 4350 1600
Wire Wire Line
	4800 1800 4350 1800
Wire Wire Line
	4800 1900 4350 1900
Wire Wire Line
	4800 2000 4350 2000
Wire Wire Line
	4800 2200 4350 2200
Wire Wire Line
	4350 1000 4800 1000
Text HLabel 4800 1000 2    60   Output ~ 0
CLOCK
Text HLabel 4800 1800 2    60   Input ~ 0
TEMP_A
Text HLabel 4800 1200 2    60   Input ~ 0
DATA_A
Text HLabel 4850 3300 2    60   Output ~ 0
HS_PWM_A
Text HLabel 4850 3000 2    60   Output ~ 0
LS_PWM_A
Text HLabel 4850 3400 2    60   Output ~ 0
HS_PWM_B
Text HLabel 4850 3100 2    60   Output ~ 0
LS_PWM_B
Text HLabel 4800 1900 2    60   Input ~ 0
TEMP_B
Text HLabel 4800 1400 2    60   Input ~ 0
DATA_B
Text HLabel 4850 3500 2    60   Output ~ 0
HS_PWM_C
Text HLabel 4850 3200 2    60   Output ~ 0
LS_PWM_C
Text HLabel 4800 2000 2    60   Input ~ 0
TEMP_C
Text HLabel 4800 1600 2    60   Input ~ 0
DATA_C
Text HLabel 4800 2200 2    60   Input ~ 0
VDC_EXT
Text HLabel 4850 3600 2    60   Output ~ 0
BRAKE_PWM
$Comp
L har-flex_THR_26 J602
U 1 1 58E84951
P 4100 3600
F 0 "J602" H 4100 4300 50  0000 C CNN
F 1 "har-flex_THR_26" V 4100 3600 50  0000 C CNN
F 2 "signal_distribution:har-flex-THR-26pin-male-straight" H 4100 2450 50  0001 C CNN
F 3 "" H 4100 2450 50  0001 C CNN
	1    4100 3600
	1    0    0    1   
$EndComp
Wire Wire Line
	4350 3000 4850 3000
Wire Wire Line
	4350 3100 4850 3100
Wire Wire Line
	4350 3200 4850 3200
Wire Wire Line
	4350 3300 4850 3300
Wire Wire Line
	4350 3400 4850 3400
Wire Wire Line
	4350 3500 4850 3500
Wire Wire Line
	4850 3600 4350 3600
$Comp
L GND #PWR040
U 1 1 58E850E4
P 3700 4300
F 0 "#PWR040" H 3700 4050 50  0001 C CNN
F 1 "GND" H 3700 4150 50  0000 C CNN
F 2 "" H 3700 4300 50  0001 C CNN
F 3 "" H 3700 4300 50  0001 C CNN
	1    3700 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 3000 3700 4300
Wire Wire Line
	4600 4200 4600 4300
Wire Wire Line
	5800 3750 5800 4000
Wire Wire Line
	6800 3700 6800 4100
Wire Wire Line
	4350 3700 4850 3700
Text HLabel 4850 3700 2    60   Input ~ 0
GATE_ENABLE
Wire Wire Line
	4350 3800 4850 3800
Wire Wire Line
	4350 3900 4850 3900
Text HLabel 4850 3800 2    60   Input ~ 0
AIN1
Text HLabel 4850 3900 2    60   Input ~ 0
AIN2
$Comp
L har-flex_THR_26 J601
U 1 1 58D2A65D
P 4100 1600
F 0 "J601" H 4100 2300 50  0000 C CNN
F 1 "har-flex_THR_26" V 4100 1600 50  0000 C CNN
F 2 "signal_distribution:har-flex-THR-26pin-male-straight" H 4100 450 50  0001 C CNN
F 3 "" H 4100 450 50  0001 C CNN
	1    4100 1600
	1    0    0    1   
$EndComp
$Comp
L GND #PWR041
U 1 1 58EABB2D
P 3700 2550
F 0 "#PWR041" H 3700 2300 50  0001 C CNN
F 1 "GND" H 3700 2400 50  0000 C CNN
F 2 "" H 3700 2550 50  0001 C CNN
F 3 "" H 3700 2550 50  0001 C CNN
	1    3700 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1000 3700 1000
Wire Wire Line
	3700 1000 3700 2550
Wire Wire Line
	3850 1100 3700 1100
Connection ~ 3700 1100
Wire Wire Line
	3850 1200 3700 1200
Connection ~ 3700 1200
Wire Wire Line
	3850 1300 3700 1300
Connection ~ 3700 1300
Wire Wire Line
	3850 1400 3700 1400
Connection ~ 3700 1400
Wire Wire Line
	3850 1500 3700 1500
Connection ~ 3700 1500
Wire Wire Line
	3850 1600 3700 1600
Connection ~ 3700 1600
Wire Wire Line
	3850 1700 3700 1700
Connection ~ 3700 1700
Wire Wire Line
	3850 1800 3700 1800
Connection ~ 3700 1800
Wire Wire Line
	3850 1900 3700 1900
Connection ~ 3700 1900
Wire Wire Line
	3850 2000 3700 2000
Connection ~ 3700 2000
Wire Wire Line
	3850 2100 3700 2100
Connection ~ 3700 2100
Wire Wire Line
	3850 2200 3700 2200
Connection ~ 3700 2200
Wire Wire Line
	4350 2100 4500 2100
Wire Wire Line
	4500 1100 4500 2400
Wire Wire Line
	4500 2400 3700 2400
Connection ~ 3700 2400
Wire Wire Line
	4350 1700 4500 1700
Connection ~ 4500 2100
Wire Wire Line
	4350 1500 4500 1500
Connection ~ 4500 1700
Wire Wire Line
	4350 1300 4500 1300
Connection ~ 4500 1500
Wire Wire Line
	4350 1100 4500 1100
Connection ~ 4500 1300
Wire Wire Line
	4350 4200 4600 4200
Wire Wire Line
	6800 4100 4350 4100
Wire Wire Line
	5800 4000 4350 4000
Wire Wire Line
	3700 4200 3850 4200
Wire Wire Line
	3850 3000 3700 3000
Connection ~ 3700 4200
Wire Wire Line
	3850 4100 3700 4100
Connection ~ 3700 4100
Wire Wire Line
	3850 4000 3700 4000
Connection ~ 3700 4000
Wire Wire Line
	3850 3900 3700 3900
Connection ~ 3700 3900
Wire Wire Line
	3850 3800 3700 3800
Connection ~ 3700 3800
Wire Wire Line
	3850 3700 3700 3700
Connection ~ 3700 3700
Wire Wire Line
	3850 3600 3700 3600
Connection ~ 3700 3600
Wire Wire Line
	3850 3500 3700 3500
Connection ~ 3700 3500
Wire Wire Line
	3850 3400 3700 3400
Connection ~ 3700 3400
Wire Wire Line
	3850 3300 3700 3300
Connection ~ 3700 3300
Wire Wire Line
	3850 3200 3700 3200
Connection ~ 3700 3200
Wire Wire Line
	3850 3100 3700 3100
Connection ~ 3700 3100
$EndSCHEMATC
