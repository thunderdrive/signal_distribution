EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ThunderDrive
LIBS:signal_distribution-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title "Enable and Shutdown Circuits"
Date "2017-04-09"
Rev "1"
Comp "ThunderDrive"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LTV-824 U701
U 1 1 58ECB72E
P 5250 2600
F 0 "U701" H 5050 2800 50  0000 L CNN
F 1 "ACPL-224" H 5100 2400 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5050 2400 50  0001 L CIN
F 3 "" H 5275 2600 50  0001 L CNN
	1    5250 2600
	-1   0    0    -1  
$EndComp
$Comp
L LTV-824 U701
U 2 1 58ECB830
P 5250 3100
F 0 "U701" H 5050 3300 50  0000 L CNN
F 1 "ACPL-224" H 5100 2900 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5050 2900 50  0001 L CIN
F 3 "" H 5275 3100 50  0001 L CNN
	2    5250 3100
	-1   0    0    -1  
$EndComp
Text HLabel 4050 2500 0    60   Output ~ 0
SHUTDOWN
Wire Wire Line
	4050 2500 4950 2500
$Comp
L GND #PWR042
U 1 1 58ECB8D0
P 4650 3450
F 0 "#PWR042" H 4650 3200 50  0001 C CNN
F 1 "GND" H 4650 3300 50  0000 C CNN
F 2 "" H 4650 3450 50  0001 C CNN
F 3 "" H 4650 3450 50  0001 C CNN
	1    4650 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 2700 4650 2700
Wire Wire Line
	4650 2700 4650 3450
$Comp
L +3V3 #PWR043
U 1 1 58ECB8ED
P 4450 2250
F 0 "#PWR043" H 4450 2100 50  0001 C CNN
F 1 "+3V3" H 4450 2390 50  0000 C CNN
F 2 "" H 4450 2250 50  0001 C CNN
F 3 "" H 4450 2250 50  0001 C CNN
	1    4450 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3000 4450 3000
Wire Wire Line
	4450 3000 4450 2250
Wire Wire Line
	4950 3200 4050 3200
Text HLabel 4050 3200 0    60   Output ~ 0
GATE_ENABLE
$Comp
L CONN_01X04 J701
U 1 1 58ECB94F
P 6550 2850
F 0 "J701" H 6550 3100 50  0000 C CNN
F 1 "EN SD" V 6650 2850 50  0000 C CNN
F 2 "Connectors_Molex:Molex_Pico-EZmate_04x1.20mm_Angled" H 6550 2850 50  0001 C CNN
F 3 "" H 6550 2850 50  0001 C CNN
	1    6550 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 2700 5700 2700
Wire Wire Line
	5700 2700 5700 2800
Wire Wire Line
	5700 2800 6350 2800
Wire Wire Line
	5550 2500 5800 2500
Wire Wire Line
	5800 2500 5800 2700
Wire Wire Line
	5550 3000 5700 3000
Wire Wire Line
	5700 3000 5700 2900
Wire Wire Line
	5700 2900 6350 2900
Wire Wire Line
	5550 3200 5800 3200
Wire Wire Line
	5800 3200 5800 3000
$Comp
L R R701
U 1 1 58EC3C7F
P 6100 2700
F 0 "R701" V 6000 2650 50  0000 C CNN
F 1 "1k" V 6100 2700 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6030 2700 50  0001 C CNN
F 3 "" H 6100 2700 50  0001 C CNN
	1    6100 2700
	0    1    1    0   
$EndComp
$Comp
L R R702
U 1 1 58EC3D2A
P 6100 3000
F 0 "R702" V 6200 2950 50  0000 C CNN
F 1 "1k" V 6100 3000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6030 3000 50  0001 C CNN
F 3 "" H 6100 3000 50  0001 C CNN
	1    6100 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	6350 2700 6250 2700
Wire Wire Line
	6250 3000 6350 3000
Wire Wire Line
	5800 2700 5950 2700
Wire Wire Line
	5800 3000 5950 3000
$Comp
L R R703
U 1 1 58EC3F28
P 4400 3350
F 0 "R703" V 4500 3300 50  0000 C CNN
F 1 "10k" V 4400 3350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4330 3350 50  0001 C CNN
F 3 "" H 4400 3350 50  0001 C CNN
	1    4400 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 3350 4650 3350
Connection ~ 4650 3350
Wire Wire Line
	4250 3350 4150 3350
Wire Wire Line
	4150 3350 4150 3200
Connection ~ 4150 3200
$EndSCHEMATC
