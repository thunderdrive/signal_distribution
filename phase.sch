EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ThunderDrive
LIBS:signal_distribution-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Single phase"
Date "2017-03-17"
Rev "2"
Comp "ThunderDrive"
Comment1 "Monster MKII"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R1
U 1 1 583819F6
P 8850 5250
F 0 "R1" V 8930 5250 50  0000 C CNN
F 1 "NTC 10k" V 8750 5250 50  0000 C CNN
F 2 "Capacitors_SMD:C_0402" V 8780 5250 50  0001 C CNN
F 3 "" H 8850 5250 50  0000 C CNN
	1    8850 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	9300 5050 9300 5250
Wire Wire Line
	9300 5250 9000 5250
Wire Wire Line
	8550 5250 8700 5250
Wire Wire Line
	8550 5150 8550 5250
Text Notes 5750 6200 0    60   ~ 0
The NC pins are Shield and must be connected to GND on the other side,\nTo asure there is no current flowing on these lines!
Wire Wire Line
	10050 4450 7000 4450
Text Label 7250 4450 0    60   ~ 0
3V3
Text Label 7250 4650 0    60   ~ 0
CLOCK
Text Label 7250 4850 0    60   ~ 0
DATA
Text Label 7250 5150 0    60   ~ 0
TEMP+
Text Label 7250 5050 0    60   ~ 0
TEMP-
Text Label 7250 5250 0    60   ~ 0
HS_PWM
Text Label 7250 5350 0    60   ~ 0
LS_PWM
Text Label 7250 5450 0    60   ~ 0
12V
Text Label 7250 5650 0    60   ~ 0
VDC
NoConn ~ 6500 4450
NoConn ~ 6500 4550
NoConn ~ 6500 4650
NoConn ~ 6500 4750
NoConn ~ 6500 4850
NoConn ~ 6500 4950
NoConn ~ 6500 5050
NoConn ~ 6500 5250
NoConn ~ 6500 5350
Text Label 7250 5550 0    60   ~ 0
GND
Wire Wire Line
	7000 5650 8150 5650
Wire Wire Line
	8050 5550 7000 5550
Wire Wire Line
	7950 5450 7000 5450
Wire Wire Line
	7850 5350 7000 5350
Wire Wire Line
	7750 5250 7000 5250
Wire Wire Line
	7000 5150 8550 5150
Wire Wire Line
	7000 5050 9300 5050
NoConn ~ 7000 4950
NoConn ~ 7000 4750
NoConn ~ 7000 4550
Wire Wire Line
	7000 4650 9950 4650
Wire Wire Line
	9850 4850 7000 4850
NoConn ~ 6500 5150
NoConn ~ 6500 5450
$Comp
L har-flex_THR_26 J1
U 1 1 58CD969D
P 6750 5050
F 0 "J1" H 6750 5750 50  0000 C CNN
F 1 "har-flex_THR_26" V 6750 5050 50  0000 C CNN
F 2 "power_bridge:har-flex-THR-26pin-male-right" H 6750 3900 50  0001 C CNN
F 3 "" H 6750 3900 50  0001 C CNN
	1    6750 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 5550 6100 5550
Wire Wire Line
	6100 5550 6100 5950
Wire Wire Line
	6100 5950 8050 5950
Connection ~ 8050 5550
Wire Wire Line
	6500 5650 6200 5650
Wire Wire Line
	6200 5650 6200 5850
Wire Wire Line
	6200 5850 7950 5850
Wire Wire Line
	7950 5850 7950 5650
Connection ~ 7950 5650
$EndSCHEMATC
