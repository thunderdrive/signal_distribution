EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ThunderDrive
LIBS:signal_distribution-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 7
Title "signal distribution board"
Date "2017-03-19"
Rev "1"
Comp "ThunderDrive"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2250 5300 1300 300 
U 58CEF553
F0 "power_supply" 60
F1 "power_supply.sch" 60
F2 "SD" I R 3550 5450 60 
$EndSheet
$Sheet
S 5950 1450 950  700 
U 58D06B88
F0 "phase_conn_A" 60
F1 "phase_conn.sch" 60
F2 "CLOCK" I L 5950 1650 60 
F3 "DATA" O L 5950 2050 60 
F4 "TEMP" O L 5950 1950 60 
F5 "HS_PWM" I L 5950 1750 60 
F6 "LS_PWM" I L 5950 1850 60 
F7 "GND_EXT" O R 6900 2050 60 
F8 "VDC_EXT" O R 6900 1950 60 
$EndSheet
$Sheet
S 5950 2450 1000 800 
U 58D10178
F0 "phase_conn_B" 60
F1 "phase_conn.sch" 60
F2 "CLOCK" I L 5950 2650 60 
F3 "DATA" O L 5950 3050 60 
F4 "TEMP" O L 5950 2950 60 
F5 "HS_PWM" I L 5950 2750 60 
F6 "LS_PWM" I L 5950 2850 60 
F7 "GND_EXT" O R 6950 3050 60 
F8 "VDC_EXT" O R 6950 2950 60 
$EndSheet
$Sheet
S 5950 3450 1000 800 
U 58D108CB
F0 "phase_conn_C" 60
F1 "phase_conn.sch" 60
F2 "CLOCK" I L 5950 3650 60 
F3 "DATA" O L 5950 4050 60 
F4 "TEMP" O L 5950 3950 60 
F5 "HS_PWM" I L 5950 3750 60 
F6 "LS_PWM" I L 5950 3850 60 
F7 "GND_EXT" O R 6950 4050 60 
F8 "VDC_EXT" O R 6950 3950 60 
$EndSheet
Wire Wire Line
	5950 1950 3600 1950
Wire Wire Line
	5950 2050 3600 2050
Wire Wire Line
	5250 2950 5950 2950
Wire Wire Line
	5150 3050 5950 3050
Wire Wire Line
	4850 3950 5950 3950
Wire Wire Line
	4750 4050 5950 4050
Wire Wire Line
	3600 1850 5950 1850
Wire Wire Line
	3600 1750 5950 1750
Wire Wire Line
	5450 2150 5450 2750
Wire Wire Line
	5450 2150 3600 2150
Wire Wire Line
	5350 2250 5350 2850
Wire Wire Line
	5350 2250 3600 2250
Wire Wire Line
	3600 2350 5250 2350
Wire Wire Line
	5250 2350 5250 2950
Wire Wire Line
	5150 3050 5150 2450
Wire Wire Line
	5150 2450 3600 2450
Wire Wire Line
	5950 3750 5050 3750
Wire Wire Line
	5050 3750 5050 2550
Wire Wire Line
	5050 2550 3600 2550
Wire Wire Line
	5950 3850 4950 3850
Wire Wire Line
	4950 3850 4950 2650
Wire Wire Line
	4950 2650 3600 2650
Wire Wire Line
	4850 3950 4850 2750
Wire Wire Line
	4850 2750 3600 2750
Wire Wire Line
	4750 4050 4750 2850
Wire Wire Line
	4750 2850 3600 2850
Wire Wire Line
	3600 1650 5950 1650
Wire Wire Line
	5350 2850 5950 2850
Wire Wire Line
	5450 2750 5950 2750
Wire Wire Line
	5950 2650 5550 2650
Wire Wire Line
	5550 1650 5550 3650
Connection ~ 5550 1650
Wire Wire Line
	5550 3650 5950 3650
Connection ~ 5550 2650
$Sheet
S 2250 1450 1350 2150
U 58D2A4E9
F0 "logic_conn" 60
F1 "logic_conn.sch" 60
F2 "CLOCK" O R 3600 1650 60 
F3 "TEMP_A" I R 3600 1950 60 
F4 "DATA_A" I R 3600 2050 60 
F5 "HS_PWM_A" O R 3600 1750 60 
F6 "LS_PWM_A" O R 3600 1850 60 
F7 "HS_PWM_B" O R 3600 2150 60 
F8 "HS_PWM_C" O R 3600 2550 60 
F9 "TEMP_B" I R 3600 2350 60 
F10 "DATA_B" I R 3600 2450 60 
F11 "LS_PWM_C" O R 3600 2650 60 
F12 "TEMP_C" I R 3600 2750 60 
F13 "DATA_C" I R 3600 2850 60 
F14 "LS_PWM_B" O R 3600 2250 60 
F15 "VDC_EXT" I R 3600 1550 60 
F16 "BRAKE_PWM" O R 3600 2950 60 
F17 "GATE_ENABLE" I R 3600 3500 60 
F18 "AIN1" I R 3600 3250 60 
F19 "AIN2" I R 3600 3350 60 
$EndSheet
$Comp
L +BATT #PWR01
U 1 1 58D1F94B
P 7200 3800
F 0 "#PWR01" H 7200 3650 50  0001 C CNN
F 1 "+BATT" H 7200 3940 50  0000 C CNN
F 2 "" H 7200 3800 50  0001 C CNN
F 3 "" H 7200 3800 50  0001 C CNN
	1    7200 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 1950 7250 1950
Wire Wire Line
	4350 1050 7650 1050
Wire Wire Line
	4350 1050 4350 1550
Wire Wire Line
	4350 1550 3600 1550
Wire Wire Line
	6950 3950 7200 3950
Wire Wire Line
	7200 3950 7200 3800
Wire Wire Line
	6950 4050 7200 4050
Wire Wire Line
	7200 4050 7200 4150
$Comp
L GND #PWR02
U 1 1 58D24931
P 7200 4150
F 0 "#PWR02" H 7200 3900 50  0001 C CNN
F 1 "GND" H 7200 4000 50  0000 C CNN
F 2 "" H 7200 4150 50  0001 C CNN
F 3 "" H 7200 4150 50  0001 C CNN
	1    7200 4150
	1    0    0    -1  
$EndComp
$Comp
L +12V #PWR03
U 1 1 58E6CE0E
P 8600 4050
F 0 "#PWR03" H 8600 3900 50  0001 C CNN
F 1 "+12V" H 8600 4190 50  0000 C CNN
F 2 "" H 8600 4050 50  0001 C CNN
F 3 "" H 8600 4050 50  0001 C CNN
	1    8600 4050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 58E6DE06
P 8700 4750
F 0 "#PWR04" H 8700 4500 50  0001 C CNN
F 1 "GND" H 8700 4600 50  0000 C CNN
F 2 "" H 8700 4750 50  0001 C CNN
F 3 "" H 8700 4750 50  0001 C CNN
	1    8700 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 4400 8600 4400
Wire Wire Line
	8600 4400 8600 4050
Wire Wire Line
	9000 4600 8700 4600
Wire Wire Line
	8700 4600 8700 4750
Wire Wire Line
	9000 4500 4650 4500
Wire Wire Line
	4650 4500 4650 2950
Wire Wire Line
	4650 2950 3600 2950
$Comp
L +3V3 #PWR05
U 1 1 58E77249
P 8850 4050
F 0 "#PWR05" H 8850 3900 50  0001 C CNN
F 1 "+3V3" H 8850 4190 50  0000 C CNN
F 2 "" H 8850 4050 50  0001 C CNN
F 3 "" H 8850 4050 50  0001 C CNN
	1    8850 4050
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 J101
U 1 1 58EC0D5F
P 9200 4450
F 0 "J101" H 9200 4700 50  0000 C CNN
F 1 "SUPPLY" V 9300 4450 50  0000 C CNN
F 2 "Connectors_Molex:Molex_Pico-EZmate_04x1.20mm_Angled" H 9200 4450 50  0001 C CNN
F 3 "" H 9200 4450 50  0001 C CNN
	1    9200 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 4300 8850 4300
Wire Wire Line
	8850 4300 8850 4050
$Sheet
S 4650 5150 1550 400 
U 58EC6B03
F0 "enable_shutdown" 60
F1 "enable_shutdown.sch" 60
F2 "SHUTDOWN" I L 4650 5450 60 
F3 "GATE_ENABLE" I L 4650 5300 60 
$EndSheet
Wire Wire Line
	4650 5300 4200 5300
Wire Wire Line
	4200 5300 4200 3500
Wire Wire Line
	4200 3500 3600 3500
Wire Wire Line
	3550 5450 4650 5450
$Comp
L CONN_01X04 J102
U 1 1 58ED1183
P 9200 5450
F 0 "J102" H 9200 5700 50  0000 C CNN
F 1 "AIN" V 9300 5450 50  0000 C CNN
F 2 "Connectors_Molex:Molex_Pico-EZmate_04x1.20mm_Angled" H 9200 5450 50  0001 C CNN
F 3 "" H 9200 5450 50  0001 C CNN
	1    9200 5450
	1    0    0    -1  
$EndComp
$Comp
L GNDA #PWR06
U 1 1 58ED20C0
P 8700 5850
F 0 "#PWR06" H 8700 5600 50  0001 C CNN
F 1 "GNDA" H 8700 5700 50  0000 C CNN
F 2 "" H 8700 5850 50  0001 C CNN
F 3 "" H 8700 5850 50  0001 C CNN
	1    8700 5850
	1    0    0    -1  
$EndComp
$Comp
L +3.3VADC #PWR07
U 1 1 58ED21D4
P 8700 5100
F 0 "#PWR07" H 8850 5050 50  0001 C CNN
F 1 "+3.3VADC" H 8700 5200 50  0000 C CNN
F 2 "" H 8700 5100 50  0001 C CNN
F 3 "" H 8700 5100 50  0001 C CNN
	1    8700 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 5300 8700 5300
Wire Wire Line
	8700 5300 8700 5100
Wire Wire Line
	9000 5600 8700 5600
Wire Wire Line
	8700 5600 8700 5850
Wire Wire Line
	9000 5400 7450 5400
Wire Wire Line
	7450 5400 7450 4600
Wire Wire Line
	7450 4600 4550 4600
Wire Wire Line
	4550 4600 4550 3250
Wire Wire Line
	4550 3250 3600 3250
Wire Wire Line
	3600 3350 4450 3350
Wire Wire Line
	4450 3350 4450 4700
Wire Wire Line
	4450 4700 7350 4700
Wire Wire Line
	7350 4700 7350 5500
Wire Wire Line
	7350 5500 9000 5500
NoConn ~ 6950 2950
NoConn ~ 6950 3050
$Comp
L R R101
U 1 1 58ED427E
P 7400 1950
F 0 "R101" V 7300 1900 50  0000 C CNN
F 1 "33k" V 7400 1950 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7330 1950 50  0001 C CNN
F 3 "" H 7400 1950 50  0001 C CNN
	1    7400 1950
	0    1    1    0   
$EndComp
$Comp
L R R102
U 1 1 58ED4653
P 7900 1950
F 0 "R102" V 7800 1900 50  0000 C CNN
F 1 "1k" V 7900 1950 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7830 1950 50  0001 C CNN
F 3 "" H 7900 1950 50  0001 C CNN
	1    7900 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 2050 8200 2050
Wire Wire Line
	7550 1950 7750 1950
Connection ~ 7650 1950
Wire Wire Line
	8050 1950 8200 1950
Wire Wire Line
	8200 1950 8200 2050
Wire Wire Line
	7650 1050 7650 1950
$EndSCHEMATC
