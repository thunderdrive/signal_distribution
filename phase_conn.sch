EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ThunderDrive
LIBS:signal_distribution-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 7
Title "Connector to single phase board"
Date "2017-03-20"
Rev "1"
Comp "ThunderDrive"
Comment1 "har-flex 26pin straight"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5200 3900 4100 3900
Wire Wire Line
	4100 3800 8900 3800
Wire Wire Line
	4100 3700 8900 3700
Wire Wire Line
	4100 3500 8900 3500
Wire Wire Line
	4100 3100 8900 3100
Wire Wire Line
	8900 3300 4100 3300
Wire Wire Line
	3200 4000 3600 4000
Wire Wire Line
	3200 2900 3200 4600
$Comp
L GND #PWR022
U 1 1 58D06D1D
P 3200 4600
AR Path="/58D06B88/58D06D1D" Ref="#PWR022"  Part="1" 
AR Path="/58D10178/58D06D1D" Ref="#PWR027"  Part="1" 
AR Path="/58D108CB/58D06D1D" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 3200 4350 50  0001 C CNN
F 1 "GND" H 3200 4450 50  0000 C CNN
F 2 "" H 3200 4600 50  0001 C CNN
F 3 "" H 3200 4600 50  0001 C CNN
	1    3200 4600
	1    0    0    -1  
$EndComp
$Comp
L har-flex_THR_26 J301
U 1 1 58D06CF1
P 3850 3500
AR Path="/58D06B88/58D06CF1" Ref="J301"  Part="1" 
AR Path="/58D10178/58D06CF1" Ref="J401"  Part="1" 
AR Path="/58D108CB/58D06CF1" Ref="J501"  Part="1" 
F 0 "J301" H 3850 4200 50  0000 C CNN
F 1 "har-flex_THR_26" V 3850 3500 50  0000 C CNN
F 2 "signal_distribution:har-flex-THR-26pin-male-straight" H 3850 2350 50  0001 C CNN
F 3 "" H 3850 2350 50  0001 C CNN
	1    3850 3500
	1    0    0    1   
$EndComp
Wire Wire Line
	3600 2900 3200 2900
Connection ~ 3200 4000
Wire Wire Line
	3600 3000 3200 3000
Connection ~ 3200 3000
Wire Wire Line
	3600 3100 3200 3100
Connection ~ 3200 3100
Wire Wire Line
	3600 3200 3200 3200
Connection ~ 3200 3200
Wire Wire Line
	3600 3300 3200 3300
Connection ~ 3200 3300
Wire Wire Line
	3600 3400 3200 3400
Connection ~ 3200 3400
Wire Wire Line
	3600 3500 3200 3500
Connection ~ 3200 3500
Wire Wire Line
	3600 3600 3200 3600
Connection ~ 3200 3600
Wire Wire Line
	3600 3700 3200 3700
Connection ~ 3200 3700
Wire Wire Line
	3600 3800 3200 3800
Connection ~ 3200 3800
Wire Wire Line
	3200 3900 3600 3900
Connection ~ 3200 3900
$Comp
L +3.3V #PWR023
U 1 1 58D070AC
P 4350 2650
AR Path="/58D06B88/58D070AC" Ref="#PWR023"  Part="1" 
AR Path="/58D10178/58D070AC" Ref="#PWR028"  Part="1" 
AR Path="/58D108CB/58D070AC" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 4350 2500 50  0001 C CNN
F 1 "+3.3V" H 4350 2790 50  0000 C CNN
F 2 "" H 4350 2650 50  0001 C CNN
F 3 "" H 4350 2650 50  0001 C CNN
	1    4350 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2900 4350 2900
Wire Wire Line
	4350 2900 4350 2650
Text HLabel 8900 3100 2    60   Input ~ 0
CLOCK
Text HLabel 8900 3300 2    60   Output ~ 0
DATA
$Comp
L +3.3VADC #PWR024
U 1 1 58D072AC
P 4750 2600
AR Path="/58D06B88/58D072AC" Ref="#PWR024"  Part="1" 
AR Path="/58D10178/58D072AC" Ref="#PWR029"  Part="1" 
AR Path="/58D108CB/58D072AC" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 4900 2550 50  0001 C CNN
F 1 "+3.3VADC" H 4750 2700 50  0000 C CNN
F 2 "" H 4750 2600 50  0001 C CNN
F 3 "" H 4750 2600 50  0001 C CNN
	1    4750 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2600 4750 3600
Wire Wire Line
	4750 3600 4100 3600
Text HLabel 8900 3500 2    60   Output ~ 0
TEMP
$Comp
L +12V #PWR025
U 1 1 58D0735D
P 5200 2650
AR Path="/58D06B88/58D0735D" Ref="#PWR025"  Part="1" 
AR Path="/58D10178/58D0735D" Ref="#PWR030"  Part="1" 
AR Path="/58D108CB/58D0735D" Ref="#PWR035"  Part="1" 
F 0 "#PWR035" H 5200 2500 50  0001 C CNN
F 1 "+12V" H 5200 2790 50  0000 C CNN
F 2 "" H 5200 2650 50  0001 C CNN
F 3 "" H 5200 2650 50  0001 C CNN
	1    5200 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3900 5200 2650
Text HLabel 8900 3700 2    60   Input ~ 0
HS_PWM
Text HLabel 8900 3800 2    60   Input ~ 0
LS_PWM
Wire Wire Line
	4650 4500 3200 4500
Wire Wire Line
	4650 3000 4650 4500
Connection ~ 3200 4500
Wire Wire Line
	4650 3000 4100 3000
Wire Wire Line
	4100 3200 4650 3200
Connection ~ 4650 3200
Wire Wire Line
	4100 3400 4650 3400
Connection ~ 4650 3400
$Comp
L R R301
U 1 1 58D30137
P 6400 4350
AR Path="/58D06B88/58D30137" Ref="R301"  Part="1" 
AR Path="/58D10178/58D30137" Ref="R401"  Part="1" 
AR Path="/58D108CB/58D30137" Ref="R501"  Part="1" 
F 0 "R301" V 6480 4350 50  0000 C CNN
F 1 "1k" V 6400 4350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6330 4350 50  0001 C CNN
F 3 "" H 6400 4350 50  0000 C CNN
	1    6400 4350
	-1   0    0    1   
$EndComp
$Comp
L LED D301
U 1 1 58D3013E
P 6400 4750
AR Path="/58D06B88/58D3013E" Ref="D301"  Part="1" 
AR Path="/58D10178/58D3013E" Ref="D401"  Part="1" 
AR Path="/58D108CB/58D3013E" Ref="D501"  Part="1" 
F 0 "D301" H 6400 4850 50  0000 C CNN
F 1 "HS" H 6400 4650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 6400 4750 50  0001 C CNN
F 3 "" H 6400 4750 50  0000 C CNN
	1    6400 4750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6400 3700 6400 4200
Wire Wire Line
	6400 4500 6400 4600
Wire Wire Line
	6400 4900 6400 5050
$Comp
L R R302
U 1 1 58D30227
P 6800 4350
AR Path="/58D06B88/58D30227" Ref="R302"  Part="1" 
AR Path="/58D10178/58D30227" Ref="R402"  Part="1" 
AR Path="/58D108CB/58D30227" Ref="R502"  Part="1" 
F 0 "R302" V 6880 4350 50  0000 C CNN
F 1 "1k" V 6800 4350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6730 4350 50  0001 C CNN
F 3 "" H 6800 4350 50  0000 C CNN
	1    6800 4350
	-1   0    0    1   
$EndComp
$Comp
L LED D302
U 1 1 58D3022E
P 6800 4750
AR Path="/58D06B88/58D3022E" Ref="D302"  Part="1" 
AR Path="/58D10178/58D3022E" Ref="D402"  Part="1" 
AR Path="/58D108CB/58D3022E" Ref="D502"  Part="1" 
F 0 "D302" H 6800 4850 50  0000 C CNN
F 1 "LS" H 6800 4650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 6800 4750 50  0001 C CNN
F 3 "" H 6800 4750 50  0000 C CNN
	1    6800 4750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6800 3800 6800 4200
Wire Wire Line
	6800 4500 6800 4600
Wire Wire Line
	6800 5050 6800 4900
$Comp
L GND #PWR026
U 1 1 58D30242
P 6600 5200
AR Path="/58D06B88/58D30242" Ref="#PWR026"  Part="1" 
AR Path="/58D10178/58D30242" Ref="#PWR031"  Part="1" 
AR Path="/58D108CB/58D30242" Ref="#PWR036"  Part="1" 
F 0 "#PWR036" H 6600 4950 50  0001 C CNN
F 1 "GND" H 6600 5050 50  0000 C CNN
F 2 "" H 6600 5200 50  0001 C CNN
F 3 "" H 6600 5200 50  0001 C CNN
	1    6600 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 5050 6800 5050
Wire Wire Line
	6600 5050 6600 5200
Connection ~ 6600 5050
Connection ~ 6400 3700
Connection ~ 6800 3800
Wire Wire Line
	3600 4100 3200 4100
Connection ~ 3200 4100
Wire Wire Line
	4100 4000 8900 4000
Wire Wire Line
	4100 4100 8900 4100
Text HLabel 8900 4000 2    60   Output ~ 0
GND_EXT
Text HLabel 8900 4100 2    60   Output ~ 0
VDC_EXT
$EndSCHEMATC
